$("a[navigate-to]").click(function(event, element) {
	
	event.preventDefault();
    
    $('html, body').animate({
        scrollTop: $($(this).attr('href')).offset().top
    }, 500);
});
