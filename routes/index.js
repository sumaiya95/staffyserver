var express = require('express');
var router = express.Router();
var request = require('request');
var path = require('path');
var url = require('url');

router.get('/:username', function(req, res, next) {
  var username = req.params.username;
  var url = 'https://www.staffy.ca/profile/'+username;
 request.get(url, function(err, response, body) {
  if(err){
    res.send(500).json(err);
  }  else {
    res.send(body);
  }
  })
})

module.exports = router;
